# arch-installation

## step 1

**keyboard layout**

    loadkeys de-latin1

**install git & dialog**

    pacman -Sy archlinux-keyring
    pacman -Sy git dialog

**clone install-scripts**

    git clone https://gitlab.com/anthe/arch-installation
    bash arch-installation/00-pre-install.sh

----

## step 2: UEFI or BIOS?

* UEFI  -> step 2a
* BIOS  -> step 2b

### step 2a: UEFI

**suggestion for partitioning**

`gpt` `new` `enter boot size (1GB)` `type / EFI-system` `write` `new` `enter` `write` `quit`


### step 2b: BIOS

**suggestion for partitioning**

`dos` `new` `enter swap size` `primary` `type / Linux-swap` `new` `enter` `primary` `bootable` `write` `quit`

----

## step 3: after reboot

**login and run install-scripts**

    bash repos/arch-installation/30-utils.sh

----

## step 4
* copy sensitive configs
* restore backups
* configure apps

---

## BUG ##
- 20-base-install: when using BIOS installing Grub on "/dev/sda" is wrong
- 30-utils: "amdgpu" not installed
- 80-mysys-setup: "rmdir Bilder Dokumente Downloads Musik Schreibtisch Videos Vorlagen Sync" did not work
- 80-mysys-setup: permissions for drives not set correctly

### fixed?
- 60-sys-setup: grub hat nich alle sed-befehle übernommen

## TODO ##
* installation via WIFI
* don't format efi, if existing
* switchuser
* create installlog.txt
  * () 2>&1 | tee installlog.txt
* switch to btrfs?
* switch to systemd-boot?
* switch to pipewire?
* switch to tkg-kernel?
* Change display-manager to xinitrc file
* exclude my sys-setup
