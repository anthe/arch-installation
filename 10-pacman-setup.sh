#!/bin/bash

echo "#################### installing reflector ####################"
pacman -Syu --noconfirm
pacman -S --noconfirm --needed reflector wget


echo "#################### configuring mirrors ####################"
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
reflector --country Germany --latest 5 --age 2 --fastest 5 --protocol https --sort rate --save /etc/pacman.d/mirrorlist


echo "#################### setting up chaotic AUR ####################" #(alternative way: https://aur.chaotic.cx)
wget -r -l1 -np -nd "https://builds.garudalinux.org/repos/chaotic-aur/x86_64/" -A "chaotic-mirrorlist-*.zst"
wget -r -l1 -np -nd "https://builds.garudalinux.org/repos/chaotic-aur/x86_64/" -A "chaotic-keyring-*.zst"
pacman -U --noconfirm chaotic-keyring-* chaotic-mirrorlist-*
rm chaotic-keyring-* chaotic-mirrorlist-*


echo "#################### configuring pacman.conf ####################"
sed -i -e 's/^#Color/Color/' -e 's/^#Para/Para/' /etc/pacman.conf
sed -i -r "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
bash -c 'printf " \n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist\n" >> /etc/pacman.conf'
pacman -Sy --noconfirm


echo "#################### configuring /etc/makepkg.conf ####################"
sed -i -e 's/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$(nproc)\"/g' -e 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -z - --threads=0)/g' -e 's/COMPRESSZST=(zstd -c -z -q -)/COMPRESSZST=(zstd -c -z -q - --threads=0)/g' -e "s/PKGEXT='.pkg.tar.xz'/PKGEXT='.pkg.tar.zst'/g" /etc/makepkg.conf


echo "#################### running base install ####################"
bash arch-installation/20-base-install.sh
