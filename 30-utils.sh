#!/bin/bash

echo "#################### installing paru AUR helper ####################"
sudo pacman -Sy
sudo pacman -S --needed --noconfirm paru


echo "#################### installing utils & enabling services & zsh ####################"
paru -S --needed --noconfirm alacritty alsa-plugins alsa-utils bat bash-completion cinnamon-translations exa fail2ban fd feh flatpak fzf git gufw hblock htop kitty librewolf linux-zen linux-zen-headers lxappearance man-db man-pages man-pages-de mlocate myxer-bin ncdu nemo nemo-fileroller nemo-preview nemo-seahorse neofetch neovim notification-daemon numlockx pacman-contrib picom polkit-gnome pulseaudio pulseaudio-alsa pulseaudio-bluetooth ripgrep rofi rofi-file-browser-extended-git rmw systemd-swap udisks2 unrar usbutils volumeicon xclip xed xfce4-notifyd xorg-xkill xorg-xprop xorg-server xorg-xinit xorg-xrandr zsh zsh-autopair-git zsh-autosuggestions zsh-completions zsh-sudo-git zsh-syntax-highlighting zsh-theme-powerlevel10k-git zsh-z-git
sudo systemctl enable systemd-swap ufw
sudo chsh $USER -s /bin/zsh

chassis=$(hostnamectl | grep Chassis | cut -f2 -d ":" | cut -f2 -d ' ')
if [ $chassis = vm ]
then
  paru -S --noconfirm --needed virtualbox-guest-utils
else
  paru -S --needed --noconfirm acpid android-file-transfer android-tools autorandr bluez bluez-utils blueman cronie downgrade grub-customizer gvfs-mtp gvfs-smb hd-idle mictray ntfs-3g playerctl pulsar preload samba smartmontools v4l2loopback-dkms-git yarn
  sudo systemctl enable acpid bluetooth cronie fstrim.timer hd-idle preload smartd
  sudo groupadd docker
  sudo groupadd vboxusers
  sudo usermod -aG docker vboxusers $USER
fi

if [ $chassis = laptop ]
then
  paru -S --needed --noconfirm cbatticon network-manager-applet tlp wireless_tools xf86-input-synaptics
fi


echo "#################### determining GPU and installing GPU drivers ####################"
if lspci | grep -E "NVIDIA|GeForce" > /dev/null
then
  paru -S --noconfirm --needed lib32-nvidia-utils lib32-vulkan-icd-loader nvidia-dkms nvidia-lts nvidia-settings nvidia-utils vulkan-icd-loader
	nvidia-xconfig
elif lspci | grep -E "Radeon" > /dev/null
then
  paru -S --noconfirm --needed lib32-mesa lib32-vulkan-radeon lib32-vulkan-icd-loader libva-mesa-driver mesa mesa-vdpau vulkan-icd-loader vulkan-radeon xf86-video-amdgpu
elif lspci | grep -E "Integrated Graphics Controller" > /dev/null
then
  paru -S --noconfirm --needed lib32-mesa lib32-vulkan-intel lib32-vulkan-icd-loader libva-intel-driver libva-utils libvdpau-va-gl vulkan-icd-loader vulkan-intel
fi


echo "#################### running next script ####################"
if [ $chassis = vm ]
then
  bash $HOME/repos/arch-installation/40-dotfiles-setup.sh
else
  bash $HOME/repos/arch-installation/31-apps.sh
fi
