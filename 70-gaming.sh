#!/bin/bash

chassis=$(hostnamectl | grep Chassis | cut -f2 -d ":" | cut -f2 -d ' ')
if [ $chassis = vm ]
then
    bash $HOME/repos/arch-installation/80-mysys-setup.sh
else
  dialog --yesno "Skip setup for games?" 0 0
  play=$?
  clear
    if [ $play = 0 ]
    then
      bash $HOME/repos/arch-installation/80-mysys-setup.sh
    else
      paru -S --needed --noconfirm python python-pip joyutils
      python3 -m keyring --disable
      pip3 install LibreGaming
      LibreGaming -b
      bash $HOME/repos/arch-installation/80-mysys-setup.sh
    fi
fi
