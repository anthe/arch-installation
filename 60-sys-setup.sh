#!/bin/bash

echo "#################### configuring grub ####################"
sudo sed -i -e 's/GRUB_DEFAULT=0/GRUB_DEFAULT=\"saved\"/' -e 's/GRUB_TIMEOUT_STYLE=\"menu\"/GRUB_TIMEOUT_STYLE=\"hidden\"/' -e 's/#GRUB_SAVEDEFAULT=\"true\"/GRUB_SAVEDEFAULT=\"true\"/' /etc/default/grub
sudo grub-mkconfig -o /boot/grub/grub.cfg


echo "#################### synching the system clock ####################"
timedatectl set-ntp true


echo "#################### setting up ZDOTDIR ####################"
sudo bash -c "echo 'export ZDOTDIR="$HOME"/.config/zsh' >> /etc/zsh/zshenv"


echo "#################### setting up German x11-KEYMAP / Change caps to esc ####################"
sudo localectl set-x11-keymap de pc105 deadgraveacute caps:escape


echo "#################### configuring swappiness ####################" #(https://wiki.archlinux.org/index.php/swap#Swappiness)
sudo bash -c "echo 'vm.swappiness = 10' > /etc/sysctl.d/99-swappiness.conf"


echo "#################### enabling SysRQ ####################" #(https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html)
sudo bash -c "echo 'kernel.sysrq = 244' > /etc/sysctl.d/99-sysctl.conf"


echo "#################### setting up journald logging / disabling coredump logging / creating swapfile ####################" #(https://wiki.archlinux.org/index.php/Swap#systemd-swap)
sudo sd '#SystemMaxUse=' 'SystemMaxUse=50M' /etc/systemd/journald.conf
sudo sd '#Storage=external' 'Storage=none' /etc/systemd/coredump.conf
sudo sd '#swapfc_enabled=0' 'swapfc_enabled=1' /etc/systemd/swap.conf


echo "#################### hardening system ####################" #(https://christitus.com/secure-linux/#security-settings-for-all-linux-installs)
cd $HOME/repos/arch-installation/61-secure-linux/
chmod +x 61-secure-linux.sh
sudo ./61-secure-linux.sh


echo "#################### setting up hblock ####################" #(https://arcolinux.com/use-hblock-to-improve-your-security-and-privacy-by-blocking-ads-tracking-and-malware-domains/)
hblock


echo "#################### changing the default terminal emulator for Nemo ####################"
gsettings set org.cinnamon.desktop.default-applications.terminal exec alacritty


echo "#################### creating polkit-rule for blueman ####################"
echo '/* Allow users in wheel group to use blueman feature requiring root without authentication */
polkit.addRule(function(action, subject) {
    if ((action.id == "org.blueman.network.setup" ||
         action.id == "org.blueman.dhcp.client" ||
         action.id == "org.blueman.rfkill.setstate" ||
         action.id == "org.blueman.pppd.pppconnect") &&
        subject.isInGroup("wheel")) {

        return polkit.Result.YES;
    }
});' | sudo tee /etc/polkit-1/rules.d/51-blueman.rules > /dev/null


echo "#################### enabling bluetooth fast connect  ####################" # https://gist.github.com/andrebrait/961cefe730f4a2c41f57911e6195e444#enable-bluetooth-fast-connect-config=
sudo sd '#FastConnectable' 'FastConnectable' /etc/bluetooth/main.conf
sudo sd '#ReconnectAttempts=7' 'ReconnectAttempts=7' /etc/bluetooth/main.conf
sudo sd '#ReconnectIntervals=1,2,3,4,8,16,32,64' 'ReconnectIntervals=1,2,3,4,8,16,32,64' /etc/bluetooth/main.conf


#echo "#################### disabling bluetooth auto power-on ####################"
#gsettings set org.blueman.plugins.powermanager auto-power-on false
#gsettings set org.blueman.general notification-daemon false


echo "#################### configuring Keychron K3 ####################"
echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode
echo "options hid_apple fnmode=0" | sudo tee -a /etc/modprobe.d/hid_apple.conf


echo "#################### seting up gaming ####################"
bash $HOME/repos/arch-installation/70-gaming.sh
