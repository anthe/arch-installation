#!/bin/bash

# choose installation disk
devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
device=$(dialog --stdout --menu "Select installation disk" 0 0 0 ${devicelist}) || exit 1

# partition and format disks, mount file system
# check if BIOS or UEFI
if [[ ! -d "/sys/firmware/efi" ]]
then
  part_swap="${device}1"
  part_root="${device}2"

  # partition the disks
  cfdisk "${device}"

  # format the partitions
  mkfs.ext4 -L arch "${part_root}"
  mkswap "${part_swap}"

  # mount the file systems
  mount -L arch /mnt/
  swapon "${part_swap}"

else
  # check if NVME
  if [[ $device =~ nvme ]]
  then
    part_boot="${device}p1"
    part_root="${device}p2"

    # partition the disks
    cfdisk "${device}"

    # format the partitions
    mkfs.fat -F 32 -n EFIBOOT "${part_boot}"
    mkfs.ext4 -L arch "${part_root}"

    # mount the file systems
    mount -L arch /mnt/
    mkdir /mnt/boot/
    mkdir /mnt/boot/efi
    mount -L EFIBOOT /mnt/boot/efi

  else
    part_boot="${device}1"
    part_root="${device}2"

    # partition the disks
    cfdisk "${device}"

    # format the partitions
    mkfs.fat -F 32 -n EFIBOOT "${part_boot}"
    mkfs.ext4 -L arch "${part_root}"

    # mount the file systems
    mount -L arch /mnt/
    mkdir /mnt/boot/
    mkdir /mnt/boot/efi
    mount -L EFIBOOT /mnt/boot/efi
  fi
fi

echo "#################### setting up parallel downloads and mirrors #################### "
sed -i 's/^#Para/Para/' /etc/pacman.conf
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
reflector --country Germany --latest 5 --age 2 --fastest 5 --protocol https --sort rate --save /etc/pacman.d/mirrorlist

echo "#################### installing base packages ####################"
pacstrap /mnt/ base dialog linux-firmware linux-lts linux-lts-headers micro

echo "#################### generating fstab ####################"
genfstab -Up /mnt/ > /mnt/etc/fstab

echo "#################### copying scripts to system ####################"
cp -r arch-installation/ /mnt/

echo "#################### chrooting ####################"
arch-chroot /mnt/ /bin/bash << EOF
bash arch-installation/10-pacman-setup.sh
EOF

# umount / reboot
umount -R /mnt
reboot
