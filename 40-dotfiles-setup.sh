#!/bin/bash
####  Creates a bare repo with my .dotfiles (https://harfangk.github.io/2016/09/18/manage-dotfiles-with-a-git-bare-repository.html) ####

echo "#################### syncing dotfiles ####################"
echo "$HOME/repos/dotfiles" >> $HOME/.gitignore
git clone --bare https://gitlab.com/$USER/dotfiles.git $HOME/repos/dotfiles/
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
mv $HOME/.bashrc $HOME/.bashrc.bak ; mv $HOME/.config/user-dirs.dirs $HOME/.config/user-dirs.dirs.bak
/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME checkout
/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME config --local status.showUntrackedFiles no
/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME config credential.helper store


# installing awesome
bash $HOME/repos/arch-installation/50-awesome.sh
