#!/bin/bash

# set variables
user=$(dialog --stdout --inputbox "Enter username" 0 0) || exit 1
: ${user:?"username cannot be empty"}

password=$(dialog --stdout --passwordbox "Enter password" 0 0) || exit 1
: ${password:?"password cannot be empty"}

hostname=$(dialog --stdout --inputbox "Enter hostname" 0 0) || exit 1
: ${hostname:?"hostname cannot be empty"}


echo "#################### setting up time zone Berlin ####################"
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc


echo "#################### setting up German localisation ####################"
printf "de_DE.UTF-8 UTF-8\nde_DE ISO-8859-1\nde_DE@euro ISO-8859-15\nen_US.UTF-8 UTF-8\n" > /etc/locale.gen
locale-gen

echo LANG=de_DE.UTF-8 >> /etc/locale.conf
printf "KEYMAP=de-latin1\nFONT=lat9w-16\n" >> /etc/vconsole.conf


echo "#################### configuring network ####################"
echo "${hostname}" >> /etc/hostname
printf "127.0.0.1	localhost\n::1 localhost\n127.0.1.1	${hostname}.localdomain	${hostname}\n" >> /etc/hosts


echo "#################### installing system packages ####################"
pacman -S --noconfirm --needed base-devel dhcpcd grub networkmanager sd xdg-user-dirs xdg-utils


echo "#################### determining processor type and installing microcode ####################"
proc_type=$(lscpu | awk '/Vendor ID:/ {print $3}')
case "$proc_type" in
	GenuineIntel)
		pacman -S --noconfirm --needed intel-ucode
		proc_ucode=intel-ucode.img
		;;
	AuthenticAMD)
		pacman -S --noconfirm --needed amd-ucode
		proc_ucode=amd-ucode.img
		;;
esac

echo "#################### determining firmware and installing bootloader ####################"
if [[ ! -d "/sys/firmware/efi" ]];
then
  grub-install --target=i386-pc /dev/sda
else
  pacman -S --noconfirm --needed efibootmgr
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch
fi
grub-mkconfig -o /boot/grub/grub.cfg

echo "#################### setting up root password ####################"
echo root:${password} | chpasswd

echo "#################### adding user ####################"
groupadd plugdev
groupadd -r autologin
useradd -mG wheel,rfkill,plugdev,autologin,lp -s /bin/bash ${user}
echo ${user}:${password} | chpasswd
echo "${user} ALL=(ALL) ALL" >> /etc/sudoers.d/${user}


echo "#################### enabling timesync service ####################"
systemctl enable NetworkManager systemd-timesyncd


echo "#################### moving installation-scripts ####################"
mkdir /home/${user}/repos/
mv /arch-installation/ /home/${user}/repos/
chown -R ${user}:${user} /home/${user}/repos/
