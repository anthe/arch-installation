#!/bin/bash

# install apps
apps=`dialog --menu "Basic or all apps?" 0 0 0 \
"basic" "" "all" "" 3>&1 1>&2 2>&3`
dialog --clear
clear

paru -S --needed --noconfirm audacious baobab bauh betterbird-de-bin catfish celluloid flameshot gpa gparted gnome-calculator gpicview hardinfo-git hunspell hunspell-de hwinfo hyphen hyphen-de keepassxc libdvdcss libmythes libreoffice-fresh libreoffice-fresh-de mintstick mythes-de simplescreenrecorder timeshift transmission-gtk tumbler ungoogled-chromium veracrypt vivaldi vivaldi-ffmpeg-codecs xfce4-appfinder xfce4-power-manager xreader
flatpak install -y com.github.tchx84.Flatseal

if [ $apps = all ]
then
  paru -S --needed --noconfirm backintime bulky detox docker docker-compose espanso-bin gping gscan2pdf gtkhash-nemo jdownloader2 meld moderndeck-bin monero-gui pix portfolio-performance-bin rclone signal-desktop solaar starship syncthing tealdeer teamviewer virtualbox virtualbox-ext-oracle virtualbox-guest-iso virtualbox-host-dkms vorta vym warp warpinator webapp-manager youtube-dl zotero-bin
  flatpak install -y com.discordapp.Discord com.microsoft.Teams com.skype.Client org.telegram.desktop us.zoom.Zoom
  sudo systemctl enable --now teamviewerd.service
fi


# run sys-setup
bash $HOME/repos/arch-installation/40-dotfiles-setup.sh
