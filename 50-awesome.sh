#!/bin/bash

echo "#################### installing displaymanager & autologin & theming ####################" #(sudo cp ..../*jpg /usr/share/backgrounds)
paru -S --needed --noconfirm lightdm lightdm-gtk-greeter #lightdm-webkit2-greeter lightdm-webkit-theme-litarvan
#sudo sed -i -e 's/#pam-service=lightdm/pam-service=lightdm/' -e 's/#pam-autologin-service=lightdm-autologin/pam-autologin-service=lightdm-autologin/' -e 's/#autologin-user=/autologin-user=$USER/' -e 's/#user-authority-in-system-dir=false/user-authority-in-system-dir=true/' -e 's/#autologin-session=/autologin-session=awesome/' /etc/lightdm/lightdm.conf
#sudo sd '#greeter-session=example-gtk-gnome' 'greeter-session=lightdm-webkit2-greeter' /etc/lightdm/lightdm.conf
#sudo sd 'webkit_theme        = antergos' 'webkit_theme        = litarvan' /etc/lightdm/lightdm-webkit2-greeter.conf


echo "#################### installing AwesomeWM ####################"
paru -S --needed --noconfirm awesome


echo "#################### syncing awesome ####################"
git clone https://gitlab.com/$USER/awesome.git $HOME/.config/awesome/


echo "#################### installing themes etc ####################"
paru -S --needed --noconfirm awesome-terminal-fonts breeze-snow-cursor-theme faba-icon-theme-git flat-remix-gtk gnome-themes-extra gruvbox-material-gtk-theme-git gtk-engine-murrine nerd-fonts-ubuntu papirus-icon-theme python-pywal qt5ct qt5-styleplugins ttf-liberation ttf-ms-fonts ttf-roboto ttf-ubuntu-font-family


echo "#################### configureing /etc/environment ####################" #(https://github.com/ChrisTitusTech/material-awesome#4-same-theme-for-qtkde-applications-and-gtk-applications-and-fix-missing-indicators)
sudo bash -c "printf 'XDG_CURRENT_DESKTOP=Unity\nQT_QPA_PLATFORMTHEME=gtk2\n' >> /etc/environment"


echo "#################### enabling lightdm ####################"
sudo systemctl enable lightdm


echo "#################### setting up system ####################"
bash $HOME/repos/arch-installation/60-sys-setup.sh
