#!/bin/bash

 echo "#################### setting up setting for specifig systems ####################"
chassis=$(hostnamectl | grep Chassis | cut -f2 -d ":" | cut -f2 -d ' ')
if [ $chassis = desktop ]
then
    bash $HOME/repos/arch-installation/81-mysys-setup-pc.sh
else
    if [ $chassis = vm ]
    then
        :
        #sudo sd "#display-setup-script=" "display-setup-script=$HOME/repos/arch-installation/82-fix-monitor-vm.sh" /etc/lightdm/lightdm.conf
    else
        sudo sd "#display-setup-script=" "display-setup-script=$HOME/repos/arch-installation/83-fix-monitor-lap.sh" /etc/lightdm/lightdm.conf
        sed -i -e "s/-- 'nm-applet --indicator',/nm-applet --indicator',/" $HOME/.config/awesome/configuration/apps.lua
    fi
    #sd 'alacritty' 'kitty' $HOME/.config/awesome/configuration/apps.lua
    sd 'anthe/desk' 'anthe/Schreibtisch' $HOME/.config/awesome/configuration/apps.lua
    sd 'pics/wallpaper' '.config/awesome/theme/wallpapers' $HOME/.bin/mypywal.sh
    reboot
fi
