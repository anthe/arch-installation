#!/bin/bash
####  just for my system ####

echo "#################### setting up cronjobs ####################" #(https://wiki.archlinux.org/index.php/Cron#Crontab_format
(crontab -l 2>/dev/null; echo "0 */6 * * * export DISPLAY=:0 && ~/.bin/mypywal.sh") | crontab -
(crontab -l 2>/dev/null; echo "10 8 * * * rclone sync /media/daten/progs megacrypt:progs") | crontab -
(crontab -l 2>/dev/null; echo "15 8 * * * rclone sync /media/daten/docs megacrypt:docs") | crontab -


echo "#################### preventing screen tearing ####################"
echo 'Section "Device"
    Identifier "AMD"
    Driver "amdgpu"
    Option "TearFree" "true"
EndSection' | sudo tee /etc/X11/xorg.conf.d/20-amdgpu.conf > /dev/null


echo "#################### configuring fstab ####################" #(https://wiki.archlinux.org/index.php/Fstab)
sudo mkdir -p /media/daten/ /media/backup/ /media/medien/ /media/nas/
sudo chown -R $USER /media/daten/ /media/backup/ /media/medien/ /media/nas/

echo "#daten
UUID=87bad67d-0f24-422b-a883-e83578b96e8b 	/media/daten 	ext4 	defaults,noatime 0 0
#backup
UUID=e00abee0-89c2-46bb-9d30-434a723f9d31 	/media/backup	ext4 	defaults,noatime 0 0
#LVM-Medien (/dev/sdb1/ /dev/sdc1)
/dev/mapper/vg1-lv1							/media/medien	ext4	defaults,noauto,noatime,users 0 0
#NAS
//192.168.178.1/nas/NAS/ 					 /media/nas       cifs      credentials=/home/$USER/.smbcredentials,vers=1.0,uid=1000,gid=1000,file_mode=0644,dir_mode=0755,noauto,users 0 0" | sudo tee -a /etc/fstab > /dev/null && sudo mount -a


# echo "#################### hiding disks ####################"
# echo 'SUBSYSTEM=="block", ENV{ID_FS_UUID}=="", ENV{UDISKS_IGNORE}="1"
# SUBSYSTEM=="block", ENV{ID_FS_UUID}=="", ENV{UDISKS_IGNORE}="1"' | sudo tee /etc/udev/rules.d/99-hide-disks.rules > /dev/null


echo "#################### linking folders ####################"
rmdir Bilder Dokumente Downloads Musik Schreibtisch Videos Vorlagen
ln -s /media/daten/pics/ /home/$USER/ ; ln -s /media/daten/docs/ /home/$USER/ ; ln -s /media/daten/dl/ /home/$USER/ ; ln -s /media/daten/progs/ /home/$USER/ ; ln -s /media/daten/desk/ /home/$USER/ ; ln -s /media/medien/home/$USER/


echo "#################### configuring firewall - allowomg kdeconnect & warpinator ####################"
sudo ufw allow 1714:1764/udp && sudo ufw allow 1714:1764/tcp
sudo ufw allow 42000/tcp && sudo ufw reload


echo "#################### enabling syncthing ####################"
systemctl --user enable --now syncthing


echo "#################### setting up Garmin permissions ####################" #(https://wiki.openstreetmap.org/wiki/DE:USB_Garmin_on_GNU/Linux)
echo '# allow Garmin USB devices read and written by a non-privileged users
SUBSYSTEM!="usb", GOTO="garmin_rules_end"
ACTION!="add", GOTO="garmin_rules_end"
ATTRS{idVendor}=="091e", ATTRS{idProduct}=="0003", MODE="0660", GROUP="plugdev"
LABEL="garmin_rules_end"' | sudo tee /etc/udev/rules.d/51-garmin.rules > /dev/null

sudo udevadm control --reload-rules


echo "#################### rebooting ####################"
reboot
